//
//  main.m
//  testConsoleApp
//
//  Created by Sergey on 7/2/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <vector>

#import <patternTest-Swift.h>
#import "FactoryMethodTest_objC.h"
#include "FactoryMethodTest_cpp.hpp"

int main(int argc, const char * argv[]) {
    @autoreleasepool
    {
        
        
        //objC
        Parent_objc* p1 = [Parent_objc makeClass];
        Parent_objc* p2 = [Son_objc makeClass];
        Parent_objc* p3 = [Daughter_objc makeClass];
        Parent_objc* p4 = [GrandSon_objc makeClass];
        Parent_objc* p5 = [GrandDaughter_objc makeClass];
        NSArray* objc = @[p1, p2, p3, p4, p5];
        for(Parent_objc* one in objc)
        {
            [one printData];
        }
    
        
        //cpp
        Parent_cpp* p11 = Parent_cpp::makeClass();
        Parent_cpp* p12 = Son_cpp::makeClass();
        Parent_cpp* p13 = Daughter_cpp::makeClass();
        Parent_cpp* p14 = GrandSon_cpp::makeClass();
        Parent_cpp* p15 = GrandDaughter_cpp::makeClass();
        std::vector<Parent_cpp*> parents = std::vector<Parent_cpp*>(5);
        parents[0] = p11;
        parents[1] = p12;
        parents[2] = p13;
        parents[3] = p14;
        parents[4] = p15;
        for(auto it = parents.begin(); it < parents.end(); ++it)
        {
            Parent_cpp* check = (*it);
            check->printData();
            delete check;
        }
        
        
        //swift
        Parent_swift* p21 = [Parent_swift makeClass];
        Parent_swift* p22 = [Son_swift makeClass];
        Parent_swift* p23 = [Daughter_swift makeClass];
        Parent_swift* p24 = [GrandSon_swift makeClass];
        Parent_swift* p25 = [GrandDaughter_swift makeClass];
        NSArray* array = @[p21, p22, p23, p24, p25];
        for(Parent_swift* one in array)
        {
            [one printData];
        }
        
    }
    return 0;
}

