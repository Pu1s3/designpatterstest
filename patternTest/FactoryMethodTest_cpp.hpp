#pragma once





class Parent_cpp
{
public:
    virtual void printData()const;
    static Parent_cpp* makeClass();
};


class Son_cpp : public Parent_cpp
{
public:
    static Parent_cpp* makeClass();
    void printData()const;
};


class Daughter_cpp : public Parent_cpp
{
public:
    static Parent_cpp* makeClass();
    void printData()const;
};


class GrandSon_cpp : public Son_cpp
{
public:
    static Parent_cpp* makeClass();
    void printData()const;
};


class GrandDaughter_cpp : public Daughter_cpp
{
public:    
    static Parent_cpp* makeClass();
    void printData()const;
};

