#import "FactoryMethodTest_objC.h"





@implementation Parent_objc : NSObject

+ (Parent_objc*)makeClass
{
    Parent_objc* obj = [self.alloc init];
    return obj;
}

- (void)printData
{
    NSLog(@"Hello, I'm ObjC %@", NSStringFromClass(self.class));
}

@end



@implementation Son_objc : Parent_objc
@end

@implementation Daughter_objc : Parent_objc
@end

@implementation GrandSon_objc : Son_objc
@end

@implementation GrandDaughter_objc : Daughter_objc
@end