
import Foundation


@objc class Parent_swift : NSObject{
    internal func printData()
    {
        print("Hello, I'm Swift ", self.className)
    }
    internal class func makeClass() -> Parent_swift
    {
        let toRet = self.init()
        return toRet
    }
    
    required override init () {
        
    }
};


@objc class Son_swift : Parent_swift{
    
};


@objc class Daughter_swift : Parent_swift{
    
};


@objc class GrandSon_swift : Son_swift{
    
};


@objc class GrandDaughter_swift :Daughter_swift{
    
};