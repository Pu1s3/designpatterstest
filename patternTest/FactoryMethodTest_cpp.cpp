#include "FactoryMethodTest_cpp.hpp"
#include <cstdio>
#include <typeinfo>

Parent_cpp* Parent_cpp::makeClass()
{
    Parent_cpp* test = new Parent_cpp;
    return test;
}

Parent_cpp* Son_cpp::makeClass()
{
    Parent_cpp* test = new Son_cpp;
    return test;
}

Parent_cpp* Daughter_cpp::makeClass()
{
    Parent_cpp* test = new Daughter_cpp;
    return test;
}

Parent_cpp* GrandDaughter_cpp::makeClass()
{
    Parent_cpp* test = new GrandDaughter_cpp;
    return test;
}

Parent_cpp* GrandSon_cpp::makeClass()
{
    Parent_cpp* test = new GrandSon_cpp;
    return test;
}



void Parent_cpp::printData()const
{
    printf("Hello, I'm C++ Parent\n");
}


void Son_cpp::printData()const
{
    printf("Hello, I'm C++ Son\n");
}


void Daughter_cpp::printData()const
{
    printf("Hello, I'm C++ Daughter\n");
}


void GrandDaughter_cpp::printData()const
{
    printf("Hello, I'm C++ GrandDaughter\n");
}


void GrandSon_cpp::printData()const
{
    printf("Hello, I'm C++ GrandSon\n");
}
