#import <Foundation/Foundation.h>




@interface Parent_objc : NSObject
+ (Parent_objc*)makeClass;
- (void)printData;
@end



@interface Son_objc : Parent_objc
@end


@interface Daughter_objc : Parent_objc
@end


@interface GrandSon_objc : Son_objc
@end


@interface GrandDaughter_objc : Daughter_objc
@end